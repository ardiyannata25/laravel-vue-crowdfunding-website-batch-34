<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
class roles extends Model
{
    use HasFactory;
    protected $table = 'roles';
    protected $primaryKey='id';
    protected $keyType = "string";
    public $timestamps = false;
    public $incrementing = false;
    protected $fillable = [ 'name'
    ];
    public function user(){
    	return $this->hasMany('App\Models\user');
    }
    protected static function boot() {
        static::creating(function ($model) {
            if ( ! $model->getKey()) {
                $model->{$model->getKeyName()} = (string) Str::uuid();
            }
        });
    }
    
}
