<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\Users;
class RegisterController extends Controller
{
    public function register(Request $r){
        Users::create([
            'username'=>$r['username'],
             'name'=>$r['name'],
             'email'=>$r['email'],
             'password'=>  Hash::make($r['password'])
         ]);
    }
   
}
