<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Str;

class otp_codes extends Authenticatable
{
    
    use HasFactory;
    protected $table = 'otp_codes';
    protected $primaryKey='id';
    protected $keyType = "string";
    public $timestamps = false;
    public $incrementing = false;
    protected $fillable = [ 'otp', 'user_id', 'valid_until' ];
    protected static function boot() {
        parent::boot();
        static::creating(function ($model) {
            if ( ! $model->getKey()) {
                $model->{$model->getKeyName()} = (string) Str::uuid();
            }
        });
    }
}
