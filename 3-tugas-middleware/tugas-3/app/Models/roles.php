<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
class roles extends Model
{
    use HasFactory;
    protected $table = 'roles';
    protected $primaryKey='id';
    protected $keyType = "string";
    public $timestamps = false;
    public $incrementing = false;
    protected $fillable = [ 'name'
    ];
    protected static function boot() {
        parent::boot();
        static::creating(function ($model) {
            if ( ! $model->getKey()) {
                $model->{$model->getKeyName()} = (string) Str::uuid();
            }
        });
    }
    public function users(){
    	return $this->hasMany('App\Models\Users');
    }
}
