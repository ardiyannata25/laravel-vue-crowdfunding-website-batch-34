<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\roles;
class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
        $data=[
            [
                'id' => "001",
                'name' => "Admin",
            ],
            [
                'id' => "002",
                'name' => "User",
            ]
        ];
        roles::insert($data);
    }
}
