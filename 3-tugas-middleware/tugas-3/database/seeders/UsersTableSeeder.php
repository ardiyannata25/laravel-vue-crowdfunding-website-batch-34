<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Users;
use Carbon\Carbon;
use Illuminate\Support\Str;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data=[
            [
                'id' => Str::uuid(),
                'username' => "andi",
                'email' => "Andi_admin@gmail.com",
                'name'=>'andi',
                'role_id'=>'001',
                'email_verified_at'=>null
            ],
            [
                'id' => Str::uuid(),
                'username' => "Hana",
                'email' => "Hana_user@Gmail.com",
                'name'=>'Hana',
                'role_id'=>'002',
                'email_verified_at'=>Carbon::now()
            ]
        ];
        Users::insert($data);
    }
}
