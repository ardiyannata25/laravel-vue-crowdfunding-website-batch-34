<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/route-1','App\Http\Controllers\authController@test1')->middleware('verified_email');

// Route::get('/route-2','App\Http\Controllers\authController@test2')->middleware('rolle_middleware');
