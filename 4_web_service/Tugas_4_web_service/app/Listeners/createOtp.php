<?php

namespace App\Listeners;

use App\Events\SendOtp;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\otp_codes;
class createOtp
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\SendOtp  $event
     * @return void
     */
    public function handle(SendOtp $event)
    {
        otp_codes::create(['user_id'=>$event->user_id,'otp','1234']) ;
    }
}
