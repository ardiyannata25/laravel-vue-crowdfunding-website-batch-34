<?php

namespace App\Models;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use App\Models\otp_codes;
class User extends Authenticatable  implements JWTSubject
{
    use HasFactory, Notifiable;
    protected $primaryKey='id';
    protected $keyType = "string";
    public $incrementing=false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function user(){
        return $this->hasOne(otp_codes::class,'user_id');
    }
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    protected static function boot() {
      parent::boot();
        
        static::creating(function ($model) {
               $id_user=(string) Str::uuid();
            if ( ! $model->getKey()) {
              
                $model->{$model->getKeyName()} =$id_user;               
            }          
        });
        // static::created(function ($model) {
        //          otp_codes::create(['user_id'=>getKey(),'otp'=>'1234']) ;      
        // });

    }
}
