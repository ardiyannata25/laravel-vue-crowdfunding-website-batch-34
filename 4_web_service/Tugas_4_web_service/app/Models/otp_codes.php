<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Carbon\Carbon;
class otp_codes extends Model
{
    
    protected $primaryKey='id';
    protected $keyType = "string";
    public $incrementing=false;
    protected $fillable = [
        'user_id', 'otp',
    ];
    protected $casts = [
        'valid_until' => 'datetime',
    ];
    use HasFactory;
    public function user(){
        return $this->hasOne(users::class,'id');
    }
    protected static function boot() {
        parent::boot();
        static::creating(function ($model) {
            if ( ! $model->getKey()) {
                $model->{$model->getKeyName()} = (string) Str::uuid();
                $model->valid_until= Carbon::now()->addMinutes(5);
            }
        });
    }
}
