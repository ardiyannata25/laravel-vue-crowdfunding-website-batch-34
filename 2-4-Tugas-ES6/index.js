//jawaban soal 1
const myFuncton = (p,l) => {
   let luas=p*l;
    let keliling=2 * (p+l);
    const x=`luas= ${luas} keliling= ${keliling}` 
    return x;
}
console.log(myFuncton(2,4));

//jawaban Soal 2
const newFunction = function literal(firstName, lastName){
    const fullName= `${firstName} ${lastName}`
    return fullName;
}
console.log(newFunction("William", "Imoh"))

//jawaban Soal 3
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
}
const {firstName, lastName,address,hobby} = newObject;
console.log(firstName, lastName, address, hobby)

// jawaban soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combined = [west, east]
console.log(combined)


//jawaban soal 5
const planet = "earth" 
const view = "glass" 
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`; 
console.log(before)